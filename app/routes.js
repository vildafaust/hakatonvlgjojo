var express  = require('express');
var router   = express.Router();
var ejs      = require ('ejs');
var mongoose = require("mongoose");
var request = require('request');


// var multipart = require('connect-multipsarty');
// multipartMiddleware = multipart({ uploadDir: './public/event' });
const fileUpload = require('express-fileupload');
const cheerio = require('cheerio')
mongoose.Promise = Promise;
const myModule = require ('./mail_notification');

var Users = require('../app/models/user.js');

module.exports = function (app, passport) {
    app.get('/', function (req, res) {
        res.render('main.ejs', {isLogin: isLoggedIn(req), role:checkPerm(req)});
    });

    app.get('/profile', function(req, res){
        if(isLoggedIn(req)){
          var api_data;
          console.log(req.user.s_name+" "+req.user.f_name+" "+req.user.t_name);
          var propertiesObject = { fio: req.user.s_name+" "+req.user.f_name+" "+req.user.t_user};
          var url = 'http://localhost/rating_all/api/get_info.php';

          request({url:url, qs:propertiesObject}, function(err, response, body) {
            if(err) { console.log(err); return; }
            //console.log(body);
            global.api_data = body;
            Users.findOneAndUpdate({'username' : req.user.username}
            ,{send_verification: true},
            {safe: true, upsert: true},
            function (err, doc) {
              res.render('lc.ejs',
            {isLogin: isLoggedIn(req),
              role:checkPerm(req),
              user: req.user,
              api_data: global.api_data,
            })})
          })}else{res.redirect('/login')}
})
    app.get('/reg', function (req, res) {
        if(!isLoggedIn(req))
        res.render('registr.ejs', {isLogin: isLoggedIn(req), message: req.flash('signupMessage'), role:checkPerm(req)});
        else res.redirect('/login');
    });
    app.post('/reg', passport.authenticate('local-signup', {
        successRedirect : '/profile', // redirect to the secure profile section
        failureRedirect : '/reg', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }))



    app.post('/login', passport.authenticate('local-login', {
        successRedirect : '/profile', // redirect to the secure profile section
        failureRedirect : '/login', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));
    app.get('/login', function(req, res){
        if(!isLoggedIn(req))
        res.render('log.ejs', {isLogin: isLoggedIn(req), message: req.flash('loginMessage'), role:checkPerm(req)});
        else res.redirect('/profile');
    });
    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });


    app.get('/404', function(req, res){ //TODO исправить на нормальную форму стр. 404
        res.redirect('/');
    });


    app.get('/rating', function(req, res){ //TODO исправить на нормальную форму стр. 404
      if(isLoggedIn(req)){
        var api_data;
        var propertiesObject = { fio: req.user.s_name + " " + req.user.f_name + " " + req.user.t_name };
        var url = 'http://localhost/rating_all/api/get_info.php';
      request({url:url, qs:propertiesObject}, function(err, response, body) {
        if(err) { console.log(err); return; }
        //console.log(body);
        global.api_data = body;
        Users.findOneAndUpdate({'username' : req.user.username}
        ,{send_verification: true},
        {safe: true, upsert: true},
        function (err, doc) {
          res.render('rating.ejs',
        {isLogin: isLoggedIn(req),
          role:checkPerm(req),
          user: req.user,
          api_data: global.api_data,
        })})
      })
    } else{
      res.render('rating.ejs',
    {isLogin: isLoggedIn(req)
    })}
    });






    function isLoggedIn(req) {
        return req.isAuthenticated();
    }
    function checkPerm(req){
      if (isLoggedIn(req)){
        role = req.user.perm
      } else {
        role = "guest"
      }
      return role
    }

};
