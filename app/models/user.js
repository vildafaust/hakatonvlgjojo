
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

var userSchema = mongoose.Schema({
    email: String,
    api_row: String,
    dispList: [[String],[Number]],
    username: String,
    password: String,
    f_name: String,
    s_name: String,
    t_name: String,
    age: Number,
    verification: {type: Boolean, default: false},
    verification_text: String,
    send_verification: {type: Boolean, default: false},
    vk: String,
    phone: String,
    date: String,
    perm: {type: String, default: 'USER '}, /* ADMIN - админ, USER - юзер */
    role: Boolean, /* True - Заказчик, False - Исполнитель. */
    orders: [String],
    urli: Boolean, // True - Юр. лицо, False - Физ. лицо.
    struct: String,
    pos: String,
    inn: String,
    kpp: String,
    naim: String,
    uradr: String,
    faadr: String,
    uradr: String,
    ruc_f_name : String,
    ruc_s_name : String,
    ruc_t_name : String,
    inst: String,
    comments : [],
    nameTeam : {type: String, default: 'none'},
});


// генерирующий хэш
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// проверка правильности пароля
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

module.exports = mongoose.model('User', userSchema);
