var express          =   require('express');
var app              =   express();
var port             =   process.env.PORT || 322;
var path             =   require('path');

var morgan           =  require('morgan');
var cookieParser     =  require('cookie-parser');
var bodyParser       =  require('body-parser');
var session          =  require('express-session');
var mongoose         =   require('mongoose');
var mongo            =   require("mongodb");
var passport         =   require('passport');
var flash            = require('connect-flash')

var configDB = require('./config/database.js');

// var multipart = require('connect-multiparty');
// var multipartMiddleware = multipart();
const fileUpload = require('express-fileupload');
//app.use(fileUpload());

//
// конфигурация
mongoose.connect(configDB.url); // соединение с БД

require('./config/passport')(passport);


app.use(session({
    secret: 'c2l4tu55x7b46js48est',
    proxy: true,
    resave: true,
    saveUninitialized: true
}));
// настрока express приложения
app.use(express.static('./'));

app.use(morgan('dev')); // запись запроса в консоль
app.use(cookieParser()); // чтение cookies
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.urlencoded({ extended: true }));

app.use(passport.initialize());
app.use(passport.session());

app.set('view engine', 'ejs'); // настройка ejs для представлений


app.use('/static', express.static(__dirname + '/public'));

app.use(flash());
require('./app/routes.js')(app,passport);

app.listen(port, function () {
    console.log('Соединение установлено. Порт ' + port);
});
