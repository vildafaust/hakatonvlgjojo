var AuthLocalStrategy = require('passport-local').Strategy;
var randomstring = require('randomstring');

var User = require('../app/models/user');

module.exports = function (passport) {

    // установка сессии passport
    // для постоянных сеансов входа в систему
    passport.serializeUser(function (user, done) {
        done(null, user.id);
    });


    passport.deserializeUser(function (id, done) {
        User.findById(id, function (err, user) {
            done(err, user);
        });
    });


    passport.use('local-login', new AuthLocalStrategy({
            usernameField: 'login',
            passwordField: 'pass',
            passReqToCallback: true
        },
        function (req, username, pass, done) {
          console.log("otpravils")


            process.nextTick(function () {
                User.findOne({'username': username}, function (err, user) {

                    // если ошибка, то возвращаем ошибку
                    if (err)
                        return done(err);
                    // если пользователь не найден, то выводится сообщение
                    if (!user)
                        return done(null, false, req.flash('loginMessage', 'Пользователь не найден'));

                    if (!user.validPassword(pass))
                        return done(null, false, req.flash('loginMessage',  'Некорректный пароль.'));
                    // если все ок, то возвращаем юзера
                    else
                        return done(null, user);
                });
            });

    }));

    // регистрация
    passport.use('local-signup', new AuthLocalStrategy({
            usernameField: 'username',
            passwordField: 'password',
            passReqToCallback: true //возвращает весь запрос на обратный вызов
        },
        function (req,  username, password, done) {
            process.nextTick(function () {
                if (!req.user) {
                    User.findOne({'username': username}, function (err, user) {
                        // if (err)
                        //     return done(err);
                        console.log("Register new User with login: ", req.body.username);
                        if (user) {
                            return done(null, false, req.flash('signupMessage', 'Логин уже занят.'));
                        } if(req.body.password != req.body.pass2){
                            return done(null, false, req.flash('signupMessage', 'Пароли должны быть одинаковые.'));
                        }
                         else {
                            var newUser = new User();
                            newUser.username = req.body.username;
                            newUser.password = newUser.generateHash(password);
                            console.log(req.body.role)
                            if(true){
                                newUser.email = req.body.iemail;
                                newUser.f_name = req.body.ifname;
                                newUser.s_name = req.body.isname;
                                newUser.t_name = req.body.itname;
                                newUser.phone = req.body.iphone;
                                newUser.inst = req.body.inst;
                                newUser.vk = req.body.vk;
                                newUser.date = req.body.date;
                                newUser.role = false;
                                newUser.perm = "ISP";
                                newUser.verification_text = randomstring.generate();
                            } else {
                                newUser.role = true;
                                newUser.perm = "ZAK";
                                if(req.body.typezak == "fiz"){
                                    newUser.urli = false;
                                    newUser.email = req.body.email;
                                    newUser.f_name = req.body.fname;
                                    newUser.s_name = req.body.sname;
                                    newUser.t_name = req.body.tname;
                                    newUser.phone = req.body.pnone;
                                    newUser.pos = req.body.pos;
                                    newUser.struct = req.body.struct;
                                    newUser.verification_text = randomstring.generate();
                                } else {
                                    newUser.urli = true;
                                    newUser.inn = req.body.inn;
                                    newUser.kpp = req.body.kpp;
                                    newUser.naim = req.body.naim;
                                    newUser.uradr = req.body.uradr;
                                    newUser.faadr = req.body.faadr;
                                    newUser.ruc_f_name = req.body.ruc_fname;
                                    newUser.ruc_s_name = req.body.ruc_sname;
                                    newUser.ruc_t_name = req.body.ruc_tname;
                                    newUser.email = req.body.zemail;
                                    newUser.f_name = req.body.zfname;
                                    newUser.s_name = req.body.zsname;
                                    newUser.t_name = req.body.ztname;
                                    newUser.phone = req.body.zpnone;
                                    newUser.pos = req.body.pos;
                                    newUser.verification_text = randomstring.generate();
                                }
                            }
                            newUser.save(function (err) {
                                if (err){
                                    return done(err);
                                } else {

                                  return done(null, newUser);
                                }


                            });
                        }
                    });
                } else {
                    // пользователь вошел в систему и имеет локальную запись. игнорирование регистрации.
                    //прежде чем создать новую учетную запись необхожимо выйти из системы
                    return done(null, req.user);
                }
            });
        }));
};
